<?php
include_once("global/error_display.php");

//use for inital test of form inputs
//exit(print_r($_POST));

//edit process code goes here...

//Get item data
$emp_id_v = $_POST['emp_id']; //needed to update correct job id


$emp_name_v = $_POST['emp_name'];
$emp_description_v = $_POST['emp_description'];
$emp_street_v = $_POST['emp_street'];
$emp_city_v = $_POST['emp_city'];
$emp_state_v = $_POST['emp_state'];
$emp_zip_v = $_POST['emp_zip'];
$emp_phone_v = $_POST['emp_phone'];
$emp_longitude_v = $_POST['emp_longitude'];
$emp_latitude_v = $_POST['emp_latitude'];
$emp_notes_v = $_POST['emp_notes'];

//exit(gettype($job_description_v) . "<br/>" . gettype($job_semester_v));
//exit($job_description_v . "," . $job_semster_v);

$pattern='/^[a-zA-Z0-9,\s\.]+$/';
$valid_street = preg_match($pattern, $emp_street_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_city = preg_match($pattern, $emp_city_v);

$pattern='/^[a-zA-Z]{2,2}+$/';
$valid_state = preg_match($pattern, $emp_state_v);

$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $emp_zip_v);

$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $emp_phone_v);


//validate inputs
if
(
empty($emp_name_v) ||
empty($emp_description_v) ||
empty($emp_street_v) ||
empty($emp_city_v) ||
empty($emp_state_v) ||
empty($emp_zip_v) ||
empty($emp_phone_v)
)
{
$error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
include('global/error.php');
}

else if ($valid_street === false)
{
echo 'Error in pattern!';
}

else if ($valid_street === 0)
{
$error = 'Street can only contain letters, numbers, commas, and periods.';
include ('global/error.php');
}

else if ($valid_city === false)
{
echo 'Error in pattern!';
}

else if ($valid_city === 0)
{
$error = 'City can only contain letters';
include ('global/error.php');
}

else if ($valid_state === false)
{
echo 'Error in pattern!';
}

else if ($valid_state === 0)
{
$error = 'State must contain 2 letters';
include ('global/error.php');
}

else if ($valid_zip === false)
{
echo 'Error in pattern!';
}

else if ($valid_zip === 0)
{
$error = 'Zip must contain 5 or 9 digits and no other characters';
include ('global/error.php');
}

else if ($valid_phone === false)
{
echo 'Error in pattern!';
}

else if ($valid_phone === 0)
{
$error = 'Phone must contain 10 digits and no other characters';
include ('global/error.php');
}







else
{
require_once "global/connection.php";

require_once "global/functions.php";


$query = 
"update employer
set
emp_name = :emp_name_p,
emp_description = :emp_description_p,
emp_street = :emp_street_p,
emp_city = :emp_city_p,
emp_state = :emp_state_p,
emp_zip = :emp_zip_p,
emp_phone = :emp_phone_p,
emp_longitude = :emp_longitude_p,
emp_latitude = :emp_latitude_p,
emp_notes = :emp_notes_p
where emp_id = :emp_id_p";

     
   

try
{
$statment = $db->prepare($query);
$statment->bindParam(':emp_id_p', $emp_id_v);
$statment->bindParam(':emp_name_p', $emp_name_v);
$statment->bindParam(':emp_description_p', $emp_description_v);
$statment->bindParam(':emp_street_p', $emp_street_v);
$statment->bindParam(':emp_city_p', $emp_city_v);
$statment->bindParam(':emp_state_p', $emp_state_v);
$statment->bindParam(':emp_zip_p', $emp_zip_v);
$statment->bindParam(':emp_phone_p', $emp_phone_v);
$statment->bindParam(':emp_longitude_p', $emp_longitude_v);
$statment->bindParam(':emp_latitude_p', $emp_latitude_v);
$statment->bindParam(':emp_notes_p', $emp_notes_v);
$row_count = $statment->execute();
$statment->closeCursor();

$last_auto_increment_id = $db->lastInsertId();

}

catch (PDOException $e)
{
$error = $e->getMessage();
display_db_error($error);
}


/*
addJob
(
	$ssu_id_v,
	$job_title_v,
	$job_description_v,
	$job_semester_v,
	$job_year_v,
	$job_recurrence_v,
	$job_credit_hours_v,
	$job_pay_v,
	$job_offer_v,
	$job_offer_accepted_v,
	$job_notes_v,
	$skl_id_v
);
*/



header('Location: index.php');
exit();	
}
?>
