<?php
//contact your Web host for DB connection documentation
//example:
$dsn = 'mysql:host=127.0.0.1;port=3306;dbname=yourdbname';
$username = 'root';
$password = 'mysql';
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

try 
{
  //instantiate new PDO connection
  $db = new PDO($dsn, $username, $password, $options);
	  //echo "Connected successfully using pdo extension!<br /><br />";
} 
catch (PDOException $e) 
{
	//only use for testing, to avoid providing security exploits
	//after testing, create custom error message
  //echo $e->getMessage();  //display error on this page
  $error = $e->getMessage(); 
  include('error.php'); //display in custom error page
  exit();
}
?>
