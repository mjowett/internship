<?php
include_once("error_display.php");

//get all jobs
function getJobs($query)
{
  //make $db available inside function
  global $db;

  //get *all* jobs sorted by job id
//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)
//$query = "SELECT * FROM job ORDER BY job_id";

//because no user entered data, no need to bind values
//$statement = $db->prepare($query);
//$statement->execute();

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
  $statement = $db->prepare($query);
  $statement->execute();
  $statement->setFetchMode(PDO::FETCH_ASSOC);
	$result = $statement->fetchAll();
/*
//or...
$result = [];
while ($row = $result->fetch_row())
	 {
        $result[] = $row;
    }
*/
	$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //get individual job
function getJob($job_id) 
{
  //make $db available inside function
  global $db;

	//find all data associated with selected job ID
  $query = 'SELECT * FROM job WHERE job_id = :job_id_p';
  try 
    {
      $statement = $db->prepare($query);
      $statement->bindParam(':job_id_p', $job_id);
      $statement->execute();
      $result = $statement->fetchAll();
      $statement->closeCursor();
      return $result;
    } 
  
  catch (PDOException $e) 
    {
      $error = $e->getMessage();
      display_db_error($error);
    }
}

  //add job
function addJob
(
	$ssu_id_v,
	$job_title_v,
	$job_description_v,
	$job_semester_v,
	$job_year_v,
	$job_recurrence_v,
	$job_credit_hours_v,
	$job_pay_v,
	$job_offer_v,
	$job_offer_accepted_v,
	$job_notes_v,
	$skl_ids_v
) 	
	
{
/*	
//check par values
exit
(
$ssu_id_v . "<br />" . 
$job_title_v . "<br />" . 
$job_description_v . "<br />" . 
$job_semester_v . "<br />" . 
$job_year_v . "<br />" . 
$job_recurrence_v . "<br />" . 
$job_credit_hours_v . "<br />" . 
$job_pay_v . "<br />" . 
$job_offer_v . "<br />" . 
$job_offer_accepted_v . "<br />" . 
$job_notes_v . "<br />" .
print_r($skl_ids_v)
);
*/	

  //make $db available inside function
  global $db;
	
	$query = 
"INSERT INTO job
(ssu_id, job_title, job_description, job_semester, job_year, job_recurrence, job_credit_hours, job_pay, job_offer, job_offer_accepted, job_notes)
VALUES
( :ssu_id_p, :job_title_p, :job_description_p, :job_semester_p, :job_year_p, :job_recurrence_p, :job_credit_hours_p, :job_pay_p, :job_offer_p, :job_offer_accepted_p, :job_notes_p)";

  try 
    {
      $statement = $db->prepare($query);

		$statement->bindParam(':ssu_id_p', $ssu_id_v);
		$statement->bindParam(':job_title_p', $job_title_v);
		$statement->bindParam(':job_description_p', $job_description_v);
		$statement->bindParam(':job_semester_p', $job_semester_v);
		$statement->bindParam(':job_year_p', $job_year_v);
		$statement->bindParam(':job_recurrence_p', $job_recurrence_v);
		$statement->bindParam(':job_credit_hours_p', $job_credit_hours_v);
		$statement->bindParam(':job_pay_p', $job_pay_v);
		$statement->bindParam(':job_offer_p', $job_offer_v);
		$statement->bindParam(':job_offer_accepted_p', $job_offer_accepted_v);
		$statement->bindParam(':job_notes_p', $job_notes_v);
			
      $statement->execute();
      $statement->closeCursor();

      // Get the last ID that was automatically generated
      $last_auto_increment_job_id = $db->lastInsertId();

      //test last auto increment id value, comment when done testing
      //exit($last_auto_increment_job_id);

			//$skl_ids_v is an array
			addJobSkill($last_auto_increment_job_id, $skl_ids_v);
		}

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //add job skill
function addJobSkill($job_id_v, $skl_ids_v) 
{
  //make $db available inside function
  global $db;

//	exit($job_id_v . ", " . print_r($skl_ids_v)); //display pars values

	//insert all associated skills for each job id
	foreach($skl_ids_v as $s) :
//exit();
		
	$query = 
"INSERT INTO job_skill
(job_id, skl_id)
VALUES
(:job_id_p, :skl_id_p)";
	
  try 
    {
      $statement = $db->prepare($query);
		$statement->bindParam(':job_id_p', $job_id_v);
		$statement->bindParam(':skl_id_p', $s);
      $statement->execute();
      $statement->closeCursor();
		}

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
	endforeach;
}

//edit job
function editJob
(
	$job_id_v,	
	$ssu_id_v,
	$job_title_v,
	$job_description_v,
	$job_semester_v,
	$job_year_v,
	$job_recurrence_v,
	$job_credit_hours_v,
	$job_pay_v,
	$job_offer_v,
	$job_offer_accepted_v,
	$job_notes_v,
	$skl_ids_v
) 	
	
{
/*	
//check par values
exit
(
$job_id_v . "<br />" . 
$ssu_id_v . "<br />" . 
$job_title_v . "<br />" . 
$job_description_v . "<br />" . 
$job_semester_v . "<br />" . 
$job_year_v . "<br />" . 
$job_recurrence_v . "<br />" . 
$job_credit_hours_v . "<br />" . 
$job_pay_v . "<br />" . 
$job_offer_v . "<br />" . 
$job_offer_accepted_v . "<br />" . 
$job_notes_v . "<br />" .
print_r($skl_ids_v)
);
*/	

  //make $db available inside function
  global $db;

  //most important: create (named) placeholders
//Note: job_entered_date will not change after inital insert!
  $query = 
  "UPDATE job
  SET
	ssu_id = :ssu_id_p,
	job_title = :job_title_p,
	job_description = :job_description_p,
	job_semester = :job_semester_p,
	job_year = :job_year_p,
	job_recurrence = :job_recurrence_p,
	job_credit_hours = :job_credit_hours_p,
	job_pay = :job_pay_p,
	job_offer = :job_offer_p,
	job_offer_accepted = :job_offer_accepted_p,
	job_notes = :job_notes_p
  WHERE job_id = :job_id_p";

  //exit($query);

  try 
    {
      $statement = $db->prepare($query);
		$statement->bindParam(':job_id_p', $job_id_v);
		$statement->bindParam(':ssu_id_p', $ssu_id_v);
		$statement->bindParam(':job_title_p', $job_title_v);
		$statement->bindParam(':job_description_p', $job_description_v);
		$statement->bindParam(':job_semester_p', $job_semester_v);
		$statement->bindParam(':job_year_p', $job_year_v);
		$statement->bindParam(':job_recurrence_p', $job_recurrence_v);
		$statement->bindParam(':job_credit_hours_p', $job_credit_hours_v);
		$statement->bindParam(':job_pay_p', $job_pay_v);
		$statement->bindParam(':job_offer_p', $job_offer_v);
		$statement->bindParam(':job_offer_accepted_p', $job_offer_accepted_v);
		$statement->bindParam(':job_notes_p', $job_notes_v);
	   $row_count = $statement->execute();		
      $statement->execute();
      $statement->closeCursor();

      //print rows affected, comment when done testing
      //exit($row_count);

			//$skl_ids_v is an array
			editJobSkill($job_id_v, $skl_ids_v);
		}

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //add job skill
function editJobSkill($job_id_v, $skl_ids_v) 
{
  //make $db available inside function
  global $db;

//	exit($job_id_v . ", " . print_r($skl_ids_v)); //display pars values

//can't update multiple skl_id will get duplicate key error!
//must first delete all skills associated with job_id, then insert!

	deleteJobSkill($job_id_v);	
	
	//edit all associated skills for each job id
	foreach($skl_ids_v as $s) :
//exit();

$query = 
"INSERT INTO job_skill
(job_id, skl_id)
VALUES
(:job_id_p, :skl_id_p)";
	
  try 
    {
      $statement = $db->prepare($query);
		$statement->bindParam(':job_id_p', $job_id_v);
		$statement->bindParam(':skl_id_p', $s);
      $statement->execute();
      $statement->closeCursor();
		}

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
	endforeach;
}

//delete job skill
function deleteJobSkill($job_id_v) 
{
  //make $db available inside function
  global $db;

  $query = 
  "DELETE FROM job_skill
  WHERE job_id = :job_id_p";

  try 
  {
    $statement = $db->prepare($query);
    $statement->bindParam(':job_id_p', $job_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();

     //view rows affected, comment when done testing
     //exit($row_count);
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

//delete job
function deleteJob($job_id_v) 
{
  //make $db available inside function
  global $db;

  $query = 
  "DELETE FROM job
  WHERE job_id = :job_id_p";

  try 
  {
    $statement = $db->prepare($query);
    $statement->bindParam(':job_id_p', $job_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();

     //view rows affected, comment when done testing
     //exit($row_count);
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //get result sets: fetchAll() returns array or arrays
function getResultSet($query)
{
  //make $db available inside function
  global $db;

//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
		$statement = $db->prepare($query);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchAll();
		$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //get result sets: fetchAll() returns array or arrays
function getResultSetByID($query, $id)
{
  //make $db available inside function
  global $db;

//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
		$statement = $db->prepare($query);
		$statement->bindParam(':id_p', $id);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //get result set: fetch() returns single array
function getResult($query)
{
  //make $db available inside function
  global $db;

//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
	//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
  $statement = $db->prepare($query);
  $statement->execute();
  $statement->setFetchMode(PDO::FETCH_ASSOC);
	$result = $statement->fetch();
	$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //get result sets: fetchAll() returns array or arrays
function getResultByID($query, $id)
{
  //make $db available inside function
  global $db;

//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
		$statement = $db->prepare($query);
		$statement->bindParam(':id_p', $id);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetch();
		$statement->closeCursor();
		return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

  //returns single column
function getColumn($query, $n)
{
  //make $db available inside function
  global $db;

//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
	//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
  $statement = $db->prepare($query);
  $statement->execute();
  $statement->setFetchMode(PDO::FETCH_ASSOC);
	$result = $statement->fetchColumn($n);
	$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    display_db_error($error);
  }
}

 //display table field metadata
 function get_enum_values($table, $field)
 {
	 $sql = "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'";
	 //exit($sql);
	 //get 2nd column values "Type"
	 $type = getColumn($sql, 1);
	 //exit(print_r($type));
	 //exit(print_r($type['Type']));

	 //return a substring from enum('fall','sum','spr')1, with only the enum values separated by commas

	 //(.*) looks for zero or more characters in between single quotation marks ('')
	 preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);													
	 /*
		 If matches is provided, filled with results of search.
			$matches[0] will contain text that matched full pattern,
			$matches[1] will have text that matched first captured parenthesized subpattern, and so on.

	 //exit(print_r($matches)); //all results
	 //exit(print_r($matches[1])); //1st captured subpattern

	 explode() returns an array of strings,
	 each of which is a substring of string formed by splitting it on boundaries formed by the string delimiter.
		*/

	 $enum = explode("','", $matches[1]); 
	 return $enum;
 }
?>
