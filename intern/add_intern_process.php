<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//no need for pst_id when adding, uses auto increment
$int_fname_v = $_POST['fname'];
$int_lname_v = $_POST['lname'];
$int_phone_v = $_POST['phone'];
$int_email1_v = $_POST['email1'];
$int_email2_v = $_POST['email2'];
$int_graduating_senior_v = $_POST['graduating_senior'];
$int_notes_v = $_POST['notes'];

$pattern='/^[a-zA-Z\s]+$/';
$valid_fname = preg_match($pattern,$int_fname_v);


$pattern='/^[a-zA-Z\s]+$/';
$valid_lname = preg_match($pattern,$int_lname_v);

$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern,$int_phone_v);



if
(
empty($int_fname_v)||
empty($int_lname_v)||
empty($int_phone_v)||
empty($int_email1_v)||
empty($int_email2_v)||
!isset($int_graduating_senior_v)
)
{
	$error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
	include('global/error.php');
}



else if($valid_fname === false)
{
	echo 'Error in pattern!';
}

else if($valid_fname === 0)
{
	$error = 'Can only contain letters!';
	include('global/error.php');
}

else if($valid_lname === false)
{
	echo 'Error in pattern!';
}

else if($valid_lname === 0)
{
	$error = 'Can only contain letters!';
	include('global/error.php');
}

else if($valid_phone === false)
{
	echo 'Error in pattern!';
}

else if($valid_phone === 0)
{
	$error = 'Phone must contain 10 digits and no other characters.';
	include('global/error.php');
}




else
{
	
require_once('global/connection.php');

$query =
"INSERT INTO intern
(int_fname,int_lname,int_phone,int_email1,int_email2,int_graduating_senior,int_notes)
VALUES
(:int_fname_v, :int_lname_v, :int_phone_v, :int_email1_v, :int_email2_v, :int_graduating_senior_v, :int_notes_v)";

try
{
	$statement = $db->prepare($query);
	$statement->bindParam(':int_fname_v',$int_fname_v);
	$statement->bindParam(':int_lname_v',$int_lname_v);
	$statement->bindParam(':int_phone_v',$int_phone_v);
	$statement->bindParam(':int_email1_v',$int_email1_v);
	$statement->bindParam(':int_email2_v',$int_email2_v);
	$statement->bindParam(':int_graduating_senior_v',$int_graduating_senior_v);
	$statement->bindParam(':int_notes_v',$int_notes_v);
	$statement->execute();
	$statement->closeCursor();
	
	$last_auto_increment_id = $db->lastInsertId();
	
}

catch(PDOEXception$e)
{
	$error = $e->getMessage();
	echo $error;
}







//include('index.php'); //forwarding is faster, one trip to server
header('Location: interns.php'); //sometimes, redirecting is needed (two trips to server)
}
?>
