<?php include_once("global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="FSU's iSchool Online Internship System.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>FSU's iSchool Online Internship System</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("global/nav.php"); ?>
	
	<div class="container">
			<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	
						</div>

						<h3>Edit Intern</h3>						
						<span style="color:red; font-weight: bold;">*</span> = required
				
						<form id="edit_intern" method="post" class="form-horizontal" action="edit_intern_process.php">
						
	<?php
	//make sure file is only required once,
	//fail causes error that stops remainder of page from precessing
	require_once "global/connection.php";
	
	//pull in function library
	require_once "global/functions.php";
		/*
	Best practice: sanitize input (prepare statements), and escape output (htmlspecialchars())
		Call htmlspecialchars() when echoing data into HTML.
		However, don't store escaped HTML in your database.
		The database should store actual data, not its HTML representation.
		Also, helps protect against cross-site scripting (XSS).
		XSS enables attackers to inject client-side script into Web pages viewed by other users
		*/
		$int_id_v = $_POST['int_id'];
		//exit($int_id_v);
								
								//find all data associated with selected petstore ID above
								$query =
								"SELECT *
								FROM intern 
								WHERE int_id = :int_id_p";
								
								$statement = $db->prepare($query);
								$statement->bindParam(':int_id_p',$int_id_v);
								$statement->execute();
								$result = $statement->fetch();
								//exit(print_r($result));
								//exit($int_fname);
								while($result != null)
								{
									//exit(print_r("test"));
	?>
	
				<input type="hidden" name="int_id" value="<?php echo $result['int_id']; ?>" />

								<div class="form-group">
										<label class="col-sm-3 control-label">*</span>First Name:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="15" name="fname" value="<?php echo ($result['int_fname']); ?>" />
										</div>
								</div>

								
								<div class="form-group">
										<label class="col-sm-3 control-label">*</span>Last Name:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="30" name="lname" value="<?php echo ($result['int_lname']); ?>" />
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">*</span>Phone:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="30" name="phone" value="<?php echo ($result['int_phone']); ?>" />
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">*</span>Email 1:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="100" name="email1" value="<?php echo ($result['int_email1']); ?>" />
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">Email 2:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="100" name="email2" value="<?php echo ($result['int_email2']); ?>" />
										</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label">
									<span style="color:red; font-weight: bold;">*</span>Graduating Senior:
									</label>
										<div class="col-sm-5">
										<select class="form-control" name="graduating_senior">
										<option value="y">Yes</option>
										<option value="n">No</option>
										
										</select>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">Notes:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="255" name="notes" />
										</div> 
								</div>
								
								<?php
								$result = $statement->fetch();
								}
								$db=null;
								?>

								<div class="form-group">
										<div class="col-sm-9 col-sm-offset-3">
												<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Update</button>
										</div>
								</div>
						</form>
<!-- 
When using the formValidation script,
Do NOT use name="submit" or id="submit" attribute for the submit button
Otherwise, the form can't be submitted after validation!
-->
				
			</div>
		</div>
	</div>
	
		
				
				
				
				
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		<!-- Turn off client-side validation, in order to test server-side validation.  -->
<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

<script type="text/javascript">

 //your client-side data validation goes here...

</script>
</body>
</html>
