<?php
//1. comment the line below, save the file, then upload to your global subdirectory on your remote Web host. (The reason to do so is that each "client" could conceivably have its own discrete set of connection values.)
//2. *after* the above step, uncomment the "local" $IP variable below, and save this file locally, so that you can develop locally.

$IP="local";

$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

//contact your Web host for DB connection documentation
//example:
if ($IP=="local")
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=internship;';
	$username = 'root';
	$password = 'mysql';
}
else
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=dbname;';
	$username = 'root';
	$password = 'mysql';
}


try 
{
  	//instantiate new PDO connection
  	$db = new PDO($dsn, $username, $password, $options);
	echo "Connected successfully using pdo extension!<br /><br />";
} 
catch (PDOException $e) 
{
	//only use for testing, to avoid providing security exploits
	//after testing, create custom error message
  	//echo $e->getMessage();  //display error on this page
  	$error = $e->getMessage(); 
  	//include('error.php'); //display in custom error page
  	die("connection failed");
  	//exit();
}

function display_db_error($error)
{
	include 'error.php';
	exit();
}
?>
