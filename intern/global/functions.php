<?php
include_once("error_display.php");


//get all jobs
function getJobs($query)
{
	//make $db available inside function
	global $db;

	try
	{

		$statement = $db->prepare($query);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchAll();

		$statement->closeCursor();
		return $result;
	}

	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}

//get individual job
function getJob($job_id)
{
	//make $db available inside function
	global $db;
	
	//find all data associated with selected job ID
	$query = 'SELECT * from job where job_id = :job_id_p';
	
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':job_id_p', $job_id);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//add job
function addJob
(
	$ssu_id_v,
	$job_title_v,
	$job_description_v,
	$job_semester_v,
	$job_year_v,
	$job_recurrence_v,
	$job_credit_hours_v,
	$job_pay_v,
	$job_offer_v,
	$job_offer_accepted_v,
	$job_notes_v,
	$skl_ids_v 
)
{
	/*
	//check par values
	exit
	(
	$ssu_id_v . "<br />" .
	$job_title_v . "<br />" .
	$job_description_v . "<br />" .
	$job_semester_v . "<br />" .
	$job_year_v . "<br />" .
	$job_recurrence_v . "<br />" .
	$job_credit_hours_v . "<br />" .
	$job_pay_v . "<br />" .
	$job_offer_v . "<br />" .
	$job_offer_accepted_v . "<br />" .
	$job_notes_v . "<br />" .
	print_r(skl_id_v)
	);
	*/ 
	
	//make db available inside function
	global $db;
	
	$query =
	"Insert into job
	(ssu_id, job_title, job_description, job_semester, job_year, job_recurrence, 
	job_credit_hours, job_pay, job_offer, job_offer_accepted, job_notes)
	Values
	( :ssu_id_p, :job_title_p, :job_description_p, :job_semester_p, :job_year_p, 
	:job_recurrence_p, :job_credit_hours_p, :job_pay_p, :job_offer_p, 
	:job_offer_accepted_p, :job_notes_p)";
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':ssu_id_p', $ssu_id_v);
		$statement->bindParam(':job_title_p', $job_title_v);
		$statement->bindParam(':job_description_p', $job_description_v);
		$statement->bindParam(':job_semester_p', $job_semester_v);
		$statement->bindParam(':job_year_p', $job_year_v);
		$statement->bindParam(':job_recurrence_p', $job_recurrence_v);
		$statement->bindParam(':job_credit_hours_p', $job_credit_hours_v);
		$statement->bindParam(':job_pay_p', $job_pay_v);
		$statement->bindParam(':job_offer_p', $job_offer_v);
		$statement->bindParam(':job_offer_accepted_p', $job_offer_accepted_v);
		$statement->bindParam(':job_notes_p', $job_notes_v);

		$statement->execute();
		$statement->closeCursor();
		
		//get the last ID that was automatically generated
		$last_auto_increment_job_id = $db->lastInsertId();
		
		
		addJobSkill($last_auto_increment_job_id, $skl_ids_v);
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//add job skill
function addJobSkill($job_id_v, $skl_ids_v)
{
	//make db available inside function
	global $db;
	
	
	//insert all associated skills for ear job id
	foreach($skl_ids_v as $s) :
	
	
	$query = 
	"INSERT into job_skill
	(job_id, skl_id)
	VALUES
	(:job_id_p, :skl_id_p)";
	
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':job_id_p',$job_id_v);
		$statement->bindParam(':skl_id_p',$s);
		
		$statement->execute();
		$statement->closeCursor();
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
	endforeach;
}


//edit job
function editJob
(	$job_id_v,	
	$ssu_id_v,
	$job_title_v,
	$job_description_v,
	$job_semester_v,
	$job_year_v,
	$job_recurrence_v,
	$job_credit_hours_v,
	$job_pay_v,
	$job_offer_v,
	$job_offer_accepted_v,
	$job_notes_v,
	$skl_ids_v 
)
{
	/*
	//check par values
	exit
	(
	$ssu_id_v . "<br />" .
	$job_title_v . "<br />" .
	$job_description_v . "<br />" .
	$job_semester_v . "<br />" .
	$job_year_v . "<br />" .
	$job_recurrence_v . "<br />" .
	$job_credit_hours_v . "<br />" .
	$job_pay_v . "<br />" .
	$job_offer_v . "<br />" .
	$job_offer_accepted_v . "<br />" .
	$job_notes_v . "<br />" .
	print_r($skl_ids_v)
	);*/
	
	
	//make db available inside function
	global $db;
	
	
	$query =
	"Update job
	set
	ssu_id = :ssu_id_p, 
	job_title = :job_title_p, 
	job_description = :job_description_p, 
	job_semester = :job_semester_p, 
	job_year = :job_year_p, 
	job_recurrence = :job_recurrence_p, 
	job_credit_hours = :job_credit_hours_p, 
	job_pay = :job_pay_p, 
	job_offer = :job_offer_p, 
	job_offer_accepted = :job_offer_accepted_p, 
	job_notes = :job_notes_p
	where job_id = :job_id_p";
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':job_id_p', $job_id_v);
		$statement->bindParam(':ssu_id_p', $ssu_id_v);
		$statement->bindParam(':job_title_p', $job_title_v);
		$statement->bindParam(':job_description_p', $job_description_v);
		$statement->bindParam(':job_semester_p', $job_semester_v);
		$statement->bindParam(':job_year_p', $job_year_v);
		$statement->bindParam(':job_recurrence_p', $job_recurrence_v);
		$statement->bindParam(':job_credit_hours_p', $job_credit_hours_v);
		$statement->bindParam(':job_pay_p', $job_pay_v);
		$statement->bindParam(':job_offer_p', $job_offer_v);
		$statement->bindParam(':job_offer_accepted_p', $job_offer_accepted_v);
		$statement->bindParam(':job_notes_p', $job_notes_v);
		$row_count = $statement->execute();
		
		$statement->execute();
		$statement->closeCursor();
		
		
		editJobSkill($job_id_v, $skl_ids_v);
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//add job skill
function editJobSkill($job_id_v, $skl_ids_v)
{
	//make db available inside function
	global $db;
	 //exit($job_id_v . ",". print_r($skl_ids_v)); //display pars values
	
	deleteJobSkill($job_id_v);
	
	foreach($skl_ids_v as $s)://exit();
	
	$query = 
	"insert into job_skill
	(job_id, skl_id)
	Values
	(:job_id_p, :skl_id_p)";
	
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':job_id_p', $job_id_v);
		$statement->bindParam(':skl_id_p', $s);
		
		$statement->execute();
		$statement->closeCursor();
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
	endforeach;
}


// delete job
function deleteJobSkill($job_id_v)
{
	//make db available inside function
	global $db;
	
	$query = 
	"delete from job_skill
	where job_id = :job_id_p";
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':job_id_p', $job_id_v);
		
		$row_count = $statement->execute();
		$statement->closeCursor();
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//delete job
function deleteJob($job_id_v)
{
	//make db available inside function
	global $db;
	
	$query = 
	"delete from job
	where job_id = :job_id_p";
	
	try
	{
		$statement = $db->prepare($query);
		
		$statement->bindParam(':job_id_p', $job_id_v);
		
		$row_count = $statement->execute();
		$statement->closeCursor();
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}//delete Internfunction deleteInt($int_id_v){	//make db available inside function	global $db;		$query = 	"delete from intern	where int_id = :int_id_p";		try	{		$statement = $db->prepare($query);				$statement->bindParam(':int_id_p', $int_id_v);				$row_count = $statement->execute();		$statement->closeCursor();	}		catch (PDOException $e)	{		$error = $e->getMessage();		display_db_error($error);	}}


//get result sets: fetchAll() returns array or arrays
function getResultSet($query)
{
	//make db available inside function
	global $db;
	
	try
	{
		$statement = $db->prepare($query);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchAll();
		$statement->closeCursor();
		
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//get result sets: fetchAll() returns array or arrays
function getResultSetByID($query, $id)
{
	//make db available inside function
	global $db;
	
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':id_p', $id);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchAll();
		$statement->closeCursor();
		
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//get result sets: fetchAll() returns array or arrays
function getResult($query)
{
	//make db available inside function
	global $db;
	
	try
	{
		$statement = $db->prepare($query);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetch();
		$statement->closeCursor();
		
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//get result sets: fetchAll() returns array or arrays
function getResultByID($query, $id)
{
	//make db available inside function
	global $db;
	
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':id_p', $id);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetch();
		$statement->closeCursor();
		
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//returns single column
function getColumn($query, $n)
{
	//make db available inside function
	global $db;
	
	try
	{
		$statement = $db->prepare($query);
		$statement->execute();
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$result = $statement->fetchColumn($n);
		$statement->closeCursor();
		
		return $result;
	}
	
	catch (PDOException $e)
	{
		$error = $e->getMessage();
		display_db_error($error);
	}
}


//display table field metadata
function get_enum_values($table, $field)
{
	$sql = "show columns from {$table} where field = '{$field}'";
	
	$type = getColumn($sql,1);
	
	preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
	
	$enum = explode("','", $matches[1]);
	
	return $enum;
}



?>
