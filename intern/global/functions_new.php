<?php
include_once('error_display.php');

//get all jobs
function getJobs($query)
{
 //make $db available inside function
 global $db;
 
 //get *all* jobs sorted by job id
//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)
//$query = "SELECT * FROM job ORDER BY job_id";

//because no user entered data, no need to bind values
//$statement = $db->prepare($query);
//$statement->execute();

try
{
//with large query reuslt sets, loop through fetch() rather than fetchAll();
	//http://php.net/manuel/en/pdostatement.bindcolumn.php

//because no user entered data, no need to bind values
$statement = $db->prepare($query);
$statement->execute();
$satement->setFetchMode(PDO::FETCH_ASSOC);
$result = $statement->fetchAll();
/*
//or..
$result = [];
while ($row = $result ->fetch_row())
	{
		$result[] = $row
	}
*/
$statement->closeCursor();
return $result;
 }

catch (PDOException $e)
 {
	$error = $e->getMessage();
	display_db_error($error);
 }
}

//get individual job
function getJob($job_id)
{
	//make $db available inside function
	global $db;
	
	//find all data associated with selected job ID
	$query = 'SELECT * FROM job WHERE job_id = :job_id_p';
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':job_id_p',$job_id):
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	}
	
	catch (PDOException $e)
	{
	 $error = $e->getMessage();
	 display_db_error($error);
	}
}

	//add job
function addJob
(
   $ssu_id_v,
   $job_title_v,
   $job_description_v,
   $job_semester_v,
   $job_year_v,
   $job_recurrence_v,
   $job_credit_hours,
   $job_pay_v,
   $job_offer_v,
   $job_offer_accepted_v,
   $job_notes_v,
   $skl_ids_v
)


 