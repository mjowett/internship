<?php include_once("../global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="FSU's iSchool Online Internship System.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>FSU's iSchool Online Internship System</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="../css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container">
			<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="page-header">
							<?php include_once("../global/header.php"); ?>	
						</div>

						<h3>Add Job</h3>						
						<span style="color:red; font-weight: bold;">*</span> = required
				
						<form id="add_job" method="post" class="form-horizontal" action="add_job_process.php">

<?php
//make sure file is only required once, 
//fail causes error that stops remainder of page from processing
require_once "../global/connection.php";

//pull in function library
require_once "../global/functions.php";
	/*
Best practice: sanitize input (perpared statements), and escape output (htmlspecialchars())		 
		 Call htmlspecialchars() when echoing data into HTML.
		 However, don't store escaped HTML in your database.
		 The database should store actual data, not its HTML representation.
		 Also, helps protect against cross-site scripting (XSS).
		 XSS enables attackers to inject client-side script into Web pages viewed by other users
	*/
?>
								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Site Supervisor:
										</label>
										<div class="col-sm-5">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT ssu_id, ssu_fname, ssu_lname FROM site_supervisor ORDER BY ssu_lname";								
												$results = getResultSet($sql);
												//print_r($results);  //for testing
												//exit();
												?>
											<select name="ssu_id" >
												<?php
												foreach($results as $result) :
												 ?>
												<option value="<?php echo $result['ssu_id']; ?>">
													<?php echo htmlspecialchars($result['ssu_lname']); ?>,
													<?php echo htmlspecialchars($result['ssu_fname']); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">
										<span style="color:red; font-weight: bold;">*</span>Skills Needed:
									</label>
										<div class="col-sm-5">
											<p class="form-control-static">(<b>*Must*</b> Ctrl + click <u>each</u> item.)</p>
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT skl_id, skl_title FROM skill ORDER BY skl_title";
												$results = getResultSet($sql);
												//print_r($results);  //for testing
												//exit();
												?>
											<select multiple name="skl_id[]" size="10">
												<?php
												foreach($results as $result) :
												 ?>
												<option value="<?php echo $result['skl_id']; ?>">
													<?php echo htmlspecialchars($result['skl_title']); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Job Title:
										</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="50" name="job_title" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Description:
										</label>
										<div class="col-sm-5">
<!--
*Be careful to remove *any* whitespace between <textarea></textarea>,
causes format issues w/MySQL client resultsets.
-->											
												<textarea class="form-control" rows="10" name="job_description"></textarea>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Semester:
										</label>
										<div class="col-sm-5">
											<select class="form-control" name="job_semester">
												<option value="fall">fall</option>
												<option value="spr">spr</option>
												<option value="sum">sum</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Year:
										</label>
										<div class="col-sm-5">
											<select name="job_year" >
												<?php
												$jobYear = 0; //Demo: 2020
												for($i=date('Y'); $i <= 2038; $i++) //initialized to current year
												{//testing only: not necessary, automatically takes first value
													//however, see how used in edit_job.php
													$selected = '';
													if ($jobYear == $i) $selected = ' selected="selected"';
													echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
												}
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Recurrence:
										</label>
										<div class="col-sm-5">
											<select class="form-control" name="job_recurrence">
												<option value="none">None</option>
												<option value="semesterly">semesterly</option>
												<option value="yearly">yearly</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Credit Hrs:
										</label>
										<div class="col-sm-5">
											<select name="job_credit_hours" >
												<?php
												for($i=1; $i < 7; $i++)
												{
													echo '<option value="'.$i.'">'.$i.'</option>';
												}
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">Pay:</label>
										<div class="col-sm-5">
											<select class="form-control" name="job_pay">
												<option value="y">Yes</option>
												<option value="n">No</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">Job Offer:</label>
										<div class="col-sm-5">
											<select class="form-control" name="job_offer">
												<option value="y">Yes</option>
												<option value="n">No</option>
												<option value="null">Unknown</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">Offer Accepted:</label>
										<div class="col-sm-5">
											<select class="form-control" name="job_offer_accepted">
												<option value="y">Yes</option>
												<option value="n">No</option>
												<option value="null">Unknown</option>
											</select>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">Notes:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="255" name="job_notes" />
										</div>
								</div>
								
								<div class="form-group">
										<div class="col-sm-9 col-sm-offset-3">
<!--
When using the formValidation script,
Do NOT use name="submit" or id="submit" attribute for the submit button.
Otherwise, the form can't be submitted after validation!
-->											
												<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
										</div>
								</div>
						</form>
					</div>
			</div>
	</div>

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		<!-- Turn off client-side validation, in order to test server-side validation.  -->
<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="../js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>

<script type="text/javascript">
$(document).ready(function() {
//http://formvalidation.io/getting-started/
//http://regexone.com/
	$('#add_job').formValidation({
			message: 'This value is not valid',
			icon: {//indicate which Font Awesome icons to use
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					ssu_id: {
							validators: {
									notEmpty: {
											message: 'Site Supervisor required.'
									},
									stringLength: {
											min: 1,
											max: 5,
											message: 'Site Supervisor ID must be no more than 5 digits.'
									},
									regexp: {//see regexp examples below (here: must include 1-5 digits)
										regexp: /^[\d{1,5}]+$/,
										message: 'Can only contain numbers.'
									},									
							},
					},
					
					'skl_id[]': {//must use ' for arrays
							validators: {
									notEmpty: {
											message: 'Skill required.'
									},
									stringLength: {
											min: 1,
											max: 5,
											message: 'Skill ID must be no more than 5 digits.'
									},
									regexp: {
										regexp: /^[\d{1,5}]+$/,
										message: 'Can only contain numbers.'
									},									
							},
					},

					job_title: {
							validators: {
									notEmpty: {
											message: 'Title required.'
									},
									stringLength: {
											min: 1,
											max: 50,
											message: 'Title no more than 50 characters.'
									},
									regexp: {
										//http://www.regular-expressions.info/
										//http://www.regular-expressions.info/quickstart.html
										//http://www.regular-expressions.info/shorthand.html
										//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
										//upper-lower alphanumeric, hyphens, underscores, periods and spaces
										//regexp: /^[a-zA-Z0-9\-_\.\s]+$/ or "^[a-zA-Z0-9\-_\.\s]*$"
										//Note: * indicates characters not required (empty string)
								     //+ indicates must include one or more characters
										//similar to: (though, \w supports other Unicode characters)
										//Here we allow period (.) as well.
										/*
										^ : start of string
										[ : beginning of character group
											a-z : any lowercase letter
											A-Z : any uppercase letter
											0-9 : any digit
											_ : underscore
										] : end of character group
										* : zero or more of the given characters
										$ : end of string
										*/
										regexp: /^[\w\-\s\.]+$/,
										message: 'Letters, numbers, hyphens, periods, and underscores only allowed.'
									},									
							},
					},

					job_description: {
							validators: {
									notEmpty: {
											message: 'Description required.'
									},
									stringLength: {
											min: 1,
											max: 1000,
											message: 'Description no more than 1000 characters.'
									},
									regexp: {
										//upper-lower alphanumeric, hyphens, underscores, periods and spaces
										//regexp: /^[a-zA-Z0-9\-_\.\s]+$/ or "^[a-zA-Z0-9\-_\.\s]*$"
								     //+ indicates must include one or more characters
										//similar to: (though, \w supports other Unicode characters)
										//Here we allow period (.) as well.
										regexp: /^[\w\-\s\.]+$/,
										message: 'Letters, numbers, hyphens, periods, and underscores only allowed.'
									},									
							},
					},

					job_semester: {
							validators: {
									notEmpty: {
											message: 'Semester required.'
									},
									stringLength: {
											min: 3,
											max: 4,
											message: 'Semester no more than 4 characters.'
									},
									regexp: {
										regexp: /^\b(fall|spr|sum)\b$/,																				
										message: 'Semester can only be fall, spr, or sum.'
									},									
							},
					},
 					
					job_year: {
							validators: {
									notEmpty: {
											message: 'Year required.'
									},
									stringLength: {
											min: 4,
											max: 4,
											message: 'Year must contain 4 digits.'
									},
									regexp: {
										regexp: /^[\d{4,4}]+$/,
										message: 'Can only contain 4-digit numbers.'
									},									
							},
					},

					job_recurrence: {
							validators: {
									notEmpty: {
											message: 'Recurrence required.'
									},
									stringLength: {
											min: 4,
											max: 10,
											message: 'Recurrence no more than 10 characters.'
									},
									regexp: {
										regexp: /^\b(none|semesterly|yearly)\b$/,
										message: 'Recurrence can only contain none, semesterly, or yearly.'
									},									
							},
					},
										
				job_credit_hours: {
							validators: {
									notEmpty: {
											message: 'Credit hours Required.'
									},
									stringLength: {
											min: 1,
											max: 1,
											message: 'Credit hours must be no more than 1 digit.'
									},
									regexp: {
										regexp: /^[1-6]+$/,
										message: 'Can only contain numbers 1 - 6.'
									},									
							},
					},
			}
	});
});
</script>
</body>
</html>
