<?php
include_once("../global/error_display.php");

//use for inital test of form inputs
//exit(print_r($_POST));

// Get item data
//no need for job_id when adding, uses auto increment
$ssu_id_v = $_POST['ssu_id'];

//skl_id returns array: if no data returned set to empty array
if(isset($_POST['skl_id'])) 
{
	$skl_id_v = $_POST['skl_id'];
}
else
{
	$skl_id_v=array();
}

//exit(print_r($skl_id_v));

$job_title_v = $_POST['job_title'];
$job_description_v = $_POST['job_description'];
$job_semester_v = $_POST['job_semester'];
$job_year_v = $_POST['job_year'];
$job_recurrence_v = $_POST['job_recurrence'];
$job_credit_hours_v = $_POST['job_credit_hours'];

//exit(gettype($job_description_v) . "<br />" . gettype($job_semester_v));
//exit($job_description_v . ", " . $job_semester_v);

if($_POST['job_pay']=='y') 
{
	$job_pay_v="y";
}
else
{
	$job_pay_v="n";
}

if($_POST['job_offer']=='y') 
{
	$job_offer_v="y";
}
else if($_POST['job_offer']=='n')
{
	$job_offer_v="n";
}
else if($_POST['job_offer']=='null')
{
	$job_offer_v=null;
}
else
{
	$job_offer_v=null;
}

if($_POST['job_offer_accepted']=='y') 
{
	$job_offer_accepted_v="y";
}
else if($_POST['job_offer_accepted']=='n')
{
	$job_offer_accepted_v="n";
}
else if($_POST['job_offer_accepted']=='null')
{
	$job_offer_accepted_v=null;
}
else
{
	$job_offer_accepted_v=null;
}

$job_notes_v = $_POST['job_notes'];

//exit(print_r($skl_id_v));

/*
//display skill ids selected
foreach($skl_id_v as $key => $value)
{
    echo "Key: " . $key . ", Value: " . $value;
}
exit();
*/

//use exit() to stop processing and test variable values. Example:
//exit($job_title_v . $job_description_v . $job_semester_v, etc.);
/*
exit
(
$ssu_id_v . "<br />" . 
$job_title_v . "<br />" . 
$job_description_v . "<br />" . 
$job_semester_v . "<br />" . 
$job_year_v . "<br />" . 
$job_recurrence_v . "<br />" . 
$job_credit_hours_v . "<br />" . 
$job_pay_v . "<br />" . 
$job_offer_v . "<br />" . 
$job_offer_accepted_v . "<br />" . 
$job_notes_v . "<br />" .
print_r($skl_id_v)
);
*/

//See PHP Regular Expressions:
//http://regexone.com/
//cheat sheet: http://www.rexegg.com/regex-quickstart.html

//ssu_id: must include 1-5 digits
$pattern='/^[\d{1,5}]+$/';
$valid_ssu_id_v = preg_match($pattern, $ssu_id_v);
//echo $valid_ssu_id_v; //test output: should be 1 (i.e., valid)

//skl_id: (preg_grep) return all array elements containing 1-5 digits (required)
$pattern='/^[\d{1,5}]+$/';
$valid_skl_id_v = preg_grep($pattern, $skl_id_v);
//exit(print_r($valid_skl_id_v)); //test output: should be 1 (i.e., valid)

//title: upper-lower alphanumeric, hyphens, underscores, periods and spaces
$pattern='/^[\w\-\s\.]+$/';
$valid_job_title_v = preg_match($pattern, $job_title_v);
//echo $valid_job_title_v; //test output: should be 1 (i.e., valid)

//description: upper-lower alphanumeric, hyphens, underscores, periods and spaces
$pattern='/^[\w\-\s\.]+$/';
$valid_job_description_v = preg_match($pattern, $job_description_v);
//echo $valid_$job_description_v; //test output: should be 1 (i.e., valid)

/*
preg_match stops looking after first match.
preg_match_all, continues to look until it finishes processing entire string.
Once match is found, it uses remainder of string to try and apply another match.
*/
//semester: match only one of 3 values: fall, spr, sum
$pattern='/^\b(fall|spr|sum)\b$/';
$valid_job_semester_v = preg_match($pattern, $job_semester_v);

//job_year: must include 4 digits
$pattern='/^[\d{4,4}]+$/';
$valid_job_year_v = preg_match($pattern, $job_year_v);
//echo $valid_job_year_v; //test output: should be 1 (i.e., valid)

//recurrence: match only one of 3 values: fall, spr, sum
$pattern='/^\b(none|semesterly|yearly)\b$/';
$valid_job_recurrence_v = preg_match($pattern, $job_recurrence_v);

//credit_hours: match only one of 6 values: 1-6
$pattern='/^[1-6]+$/';
$valid_job_credit_hours_v = preg_match($pattern, $job_credit_hours_v);
//echo $valid_credit_hours; //test output: should be 1 (i.e., valid)

//pay: match only one of 2 values: n/y
$pattern='/^[ny]+$/';
$valid_job_pay_v = preg_match($pattern, $job_pay_v);

//offer: match only one of 3 values: n/y/null
$pattern='/^[ny]*$/';
$valid_job_offer_v = preg_match($pattern, $job_offer_v);

//offer: match only one of 3 values: n/y/null
$pattern='/^[ny]*$/';
$valid_job_offer_accepted_v = preg_match($pattern, $job_offer_accepted_v);

//validate inputs - must contain all required fields
/*
Variable is set if it has been assigned a value other than NULL.
Even if a variable is assigned an empty string (""), it is set.

Variable is empty if it is an empty string (""), 0, "0", false, NULL, array(), and a variable declared but not given a value are all empty.

empty() function: returns true for the following values:
empty string (""), 0, "0", NULL, or FALSE

isset() function: returns false when the variable is NULL, that is, not initialized or given a value.*/

//trim then check length of textarea: if!strlen(trim($job_description_v)) NO DATA!
//http://stackoverflow.com/questions/8283564/how-do-you-check-if-textarea-is-empty
if 
(
	empty($ssu_id_v) ||
	empty($job_title_v) ||
	!strlen(trim($job_description_v)) ||
	empty($job_semester_v) ||
	empty($job_year_v) ||
	empty($job_recurrence_v) ||
	empty($job_credit_hours_v) ||
	empty($skl_id_v)
) 
{
  $error = "Please check <span style='color:red; font-weight: bold;'>*</span>required fields and try again.";
//	header('Location: ../global/error.php');
  include('../global/error.php');
} 

else if ($valid_ssu_id_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_ssu_id_v === 0)
{
  $error = 'ssu_id can only contain integers, from 1 to 5 digits.';
  include('../global/error.php');
} 

else if ($valid_skl_id_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_skl_id_v === 0)
{
  $error = 'skl_id can only contain integers, from 1 to 5 digits.';
  include('../global/error.php');
} 

else if ($valid_job_title_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_title_v === 0)
{
  $error = 'Title can only contain letters, numbers, hyphens, periods, and underscores.';
  include('../global/error.php');
} 

else if ($valid_job_description_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_description_v === 0)
{
  $error = 'Title can only contain letters, numbers, hyphens, periods, and underscores.';
  include('../global/error.php');
} 

else if ($valid_job_semester_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_semester_v === 0)
{
  $error = 'Semester can only be fall, spr, or sum.';
  include('../global/error.php');
} 

else if ($valid_job_year_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_year_v === 0)
{
  $error = 'Year must be 4 digits.';
  include('../global/error.php');
} 

else if ($valid_job_recurrence_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_recurrence_v === 0)
{
  $error = 'Recurrence can only be none, semesterly, or yearly.';
  include('../global/error.php');
} 

else if ($valid_job_credit_hours_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_credit_hours_v === 0)
{
  $error = 'Credit hours must be between 1-6 hrs, inclusive.';	
  include('../global/error.php');
} 

//redundant test: though, demos another method of testing input
//credit_hours: must contain numbers between 1-6, inclusive
else if
(
	 !is_numeric($job_credit_hours_v) ||
	 $job_credit_hours_v < 1 ||
	 $job_credit_hours_v > 6
) 
{
  $error = 'Credit hours must be between 1-6 hrs, inclusive.';
  include('../global/error.php');
}

else if ($valid_job_pay_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_pay_v === 0)
{
  $error = 'Pay can only be y, n.';
  include('../global/error.php');
} 

else if ($valid_job_offer_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_offer_v === 0)
{
  $error = 'Offer can only be y, n, or null.';
  include('../global/error.php');
} 

else if ($valid_job_offer_accepted_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_job_offer_accepted_v === 0)
{
  $error = 'Offer accepted can only be y, n, or null.';
  include('../global/error.php');
} 

else 
{
// If valid, add record
  //make sure file is only required once, 
  //fail causes error that stops remainder of page from processing
  require_once "../global/connection.php";

  //pull in function library
  require_once "../global/functions.php";

  //call function, passing arguments that contain data values using variables (no need for job_id, as it is auto_increment)

  addJob
	(
		$ssu_id_v,
		$job_title_v,
		$job_description_v,
		$job_semester_v,
		$job_year_v,
		$job_recurrence_v,
		$job_credit_hours_v,
		$job_pay_v,
		$job_offer_v,
		$job_offer_accepted_v,
		$job_notes_v,
		$skl_id_v
	);

//demo problems with include(), when refreshing browser after processing addJob()
//include('index.php'); 

//instead, use...
header('Location: index.php');
exit();
}
?>
