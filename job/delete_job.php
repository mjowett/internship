<?php
include_once("../global/error_display.php");

// Get item ID
$job_id_v = $_POST['job_id'];

//  validate input - must contain required field
if (empty($job_id_v)) 
{
  $error = "Invalid data. Check field and try again.";
  include('../global/error.php');
} 

else 
{
// If valid, delete
  //make sure file is only required once, 
  //fail causes error that stops remainder of page from processing
  require_once('../global/connection.php');

  //pull in function library
  require_once "../global/functions.php";

  //call function, passing arguments that contain data values using variables
  deleteJob($job_id_v);

include('index.php');
}
?>
