<?php include_once("../global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="FSU's iSchool Online Internship System.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>FSU's iSchool Online Internship System</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="../css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>

				<h3>Edit Job</h3>						
				<span style="color:red; font-weight: bold;">*</span> = required
				
				<form id="edit_job" method="post" class="form-horizontal" action="edit_job_process.php">

					<?php
						/*
							 Use hidden form field to pass back job ID with other displayed attribute values, to database.
							Also, don't put these types of comments in HTML comments, can be
							viewed when looking through HTML source code.

							Lastly, don't permit user to modify pk (job_id), only allow modifications to the associated data.
						*/

						//make sure file is only required once, 
						//fail causes error that stops remainder of page from processing
					require_once "../global/connection.php";

/*
Best practice: sanitize input (prepared statements), and escape output (htmlspecialchars())
		 Call htmlspecialchars() when echoing data into HTML.
		 However, don't store escaped HTML in your database.
		 (There are security issues associated with doing so, as well: http://php.net/manual/en/function.addslashes.php)
		 The database should store actual data, not its HTML representation.
		 Also, htmlspecialchars() helps protect against cross-site scripting (XSS).
		 Furthermore, XSS enables attackers to inject client-side script into Web pages viewed by other users.
		 Lastly, prepared statements help prevent SQL injection, because the query
		 and the data for the query are separately sent to the SQL server.
*/
						
						//pull in function library
					require_once "../global/functions.php";

						//capture ID from index.php page
					$job_id_v = htmlspecialchars($_POST['job_id']);						

						//call UDF (user-defined function)
					$results = getJob($job_id_v);
															
					//for testing
					//print_r($results);
					//exit(print_r($results));
					
					//get scalar values
					foreach($results as $result) :
						$job_title_v = htmlspecialchars($result['job_title']);
						$job_description_v = htmlspecialchars($result['job_description']);
						$job_notes_v = htmlspecialchars($result['job_notes']);
					endforeach;
?>
								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Site Supervisor:
										</label>
										<div class="col-sm-5">
												<?php
												//call UDF (user-defined function)
												//select ssu_id for specific job/internship
												//$sql = "SELECT ssu_id FROM site_supervisor natural join job where job_id = :id_p";
												$sql = "SELECT ssu_id FROM site_supervisor natural join job where job_id = :id_p";
												//exit($sql);
												$ssu_id_v = getResultSetByID($sql, $job_id_v); //fetchAll() returns array of arrays
												//exit(print_r($ssu_id_v));												

												//select all site supervisors (compare any matches below)
												$sql = "SELECT ssu_id, ssu_fname, ssu_lname FROM site_supervisor ORDER BY ssu_lname";
												$results = getResultSet($sql);
												//exit(print_r($results));
												?>
											<select name="ssu_id" >
												<?php
												foreach($results as $result) : //display all site supervisors
												?>
													<option value="<?php echo $result['ssu_id']; ?>"
																	<?php
																	//display match in site supervisor for specific job
																	//against all site supervisors
																	foreach($ssu_id_v as $ssu=>$s) :
																	//exit(print_r($s));
																	if ($s['ssu_id'] == $result['ssu_id'])
																	{
																		echo 'selected="selected"';
																	}
																	endforeach;
																	?>>
													<?php echo htmlspecialchars($result['ssu_lname']); ?>,
													<?php echo htmlspecialchars($result['ssu_fname']); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Skills Needed:
										</label>
										<div class="col-sm-5">
											<p class="form-control-static">(<b>*Must*</b> Ctrl + click <u>each</u> item.)</p>
												<?php
												//call UDF (user-defined function)
												$sql = "select s.skl_id
																from skill s
																		natural join job_skill
																where job_id= :id_p";

												//exit($sql);
												$skl_id_v = getResultSetByID($sql, $job_id_v); //fetchAll() returns array of arrays
												//exit(print_r($ssu_id_v));
												
												$sql = "SELECT skl_id, skl_title FROM skill ORDER BY skl_title";
												$results = getResultSet($sql); //fetchAll() returns array of arrays
												//exit(print_r($results));
												?>

											<select multiple name="skl_id[]" size="10">
												<?php
												foreach($results as $result) :
												?>
													<option value="<?php echo $result['skl_id']; ?>"
																	<?php
																	//check for selected skl_id
																	foreach($skl_id_v as $skl=>$s) :
																	//exit(print_r($s));
																	if ($s['skl_id'] == $result['skl_id'])
																	{
																		echo 'selected="selected"';
																	}
																	endforeach;
																	?>>
													<?php echo htmlspecialchars($result['skl_title']); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>
								
							  <input type="hidden" name="job_id" value="<?php echo $job_id_v; ?>" />

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Job Title:
										</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="50" name="job_title" value="<?php echo $job_title_v; ?>" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Description:
										</label>
										<div class="col-sm-5">
<!--
*Be careful to remove *any* whitespace between <textarea></textarea>,
causes format issues w/MySQL client resultsets.
-->
											<textarea class="form-control" rows="10" name="job_description"><?php echo $job_description_v; ?></textarea>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Semester:
										</label>
										<div class="col-sm-5">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_semester FROM job where job_id = :id_p";
												//exit($sql);
												$job_semester_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_semester_v));												
												
												//variables used to return table column values
												$table="job";
												$field= "job_semester";
												$results = get_enum_values($table,$field);
												//exit(print_r($results));

												/*
													 //print all enum values
												foreach($results as $result=>$r) :
																	 echo $r;
												endforeach;
												exit();
												*/

												/*
													 //print enum value for selected job_id
												foreach($job_semester_v as $semester=>$s) :
																	 echo $s;
												endforeach;
												exit();
													*/

												?>
											<select name="job_semester">
												<?php
												foreach($results as $result=>$r) :
												?>
													<option value="<?php echo $r; ?>"
																	<?php
																	//check for selected job_semester
																	foreach($job_semester_v as $semester=>$s) :
																	//exit($r);
																	//exit($s);
																	if ($s == $r)
																	{
																		echo 'selected="selected"';
																	}
																	endforeach;
																	?>>
													<?php echo htmlspecialchars($r); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Year:
										</label>
										<div class="col-sm-5">
											<select name="job_year" >
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_year FROM job where job_id = :id_p";
												//exit($sql);

												$job_year_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_year_v));												

												foreach($job_year_v as $year=>$jobYear) :
												//exit($jobYear);
												endforeach;
												
												for($i=2005; $i <= 2038; $i++)
												{//years begin with 2005, but $jobYear will display
													$selected = '';
													if ($jobYear == $i) $selected = ' selected="selected"';
													echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
												}
												?>
											</select>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Recurrence:
										</label>
										<div class="col-sm-5">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_recurrence FROM job where job_id = :id_p";
												//exit($sql);
												$job_recurrence_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_recurrence_v));												

												//variables used to return table column values
												$table="job";
												$field= "job_recurrence";
												$results = get_enum_values($table,$field);
												//exit(print_r($results));
												
												?>
											<select name="job_recurrence">
												<?php
												foreach($results as $result=>$r) :
												?>
													<option value="<?php echo $r; ?>"
																	<?php
																	//check for selected job_recurrence
																	foreach($job_recurrence_v as $recurrence=>$s) :
																	//exit(print_r($s));
																	if ($s == $r)
																	{
																		echo 'selected="selected"';
																	}
																	endforeach;
																	?>>
													<?php echo htmlspecialchars($r); ?>
												</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">
											<span style="color:red; font-weight: bold;">*</span>Credit Hrs:
										</label>
										<div class="col-sm-5">
											<select name="job_credit_hours" >
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_credit_hours FROM job where job_id = :id_p";
												//exit($sql);

												$job_credit_hours_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_year_v));												

												foreach($job_credit_hours_v as $hours=>$creditHour) :
												//exit($jobYear);
												endforeach;
												
												for($i=1; $i < 7; $i++)
												{//testing only: not necessary, automatically takes first value
													$selected = '';
													if ($creditHour == $i) $selected = ' selected="selected"';
													echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
												}
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">Status:</label>
										<div class="col-sm-5">
											<label class="checkbox-inline">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_pay FROM job where job_id = :id_p";
												//exit($sql);

												$job_pay_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_pay_v));												

												foreach($job_pay_v as $pay=>$jobPay) :
												//exit($jobPay);
												endforeach;

//Note: In XHTML, attribute minimization is forbidden, checked attribute must be defined as checked="checked"	
												?>
												<input type="checkbox" name="job_pay" value="y"
															 <?php echo ($jobPay=='y' ? 'checked="checked"' : '');?>> Pay
											</label>

											<label class="checkbox-inline">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_offer FROM job where job_id = :id_p";
												//exit($sql);

												$job_offer_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_offer_v));												

												foreach($job_offer_v as $offer=>$jobOffer) :
												//exit($jobOffer);
												endforeach;
												?>
												<input type="checkbox" name="job_offer" value="y"
															 <?php echo ($jobOffer=='y' ? 'checked="checked"' : '');?>> Job Offer
											</label>

											<label class="checkbox-inline">
												<?php
												//call UDF (user-defined function)
												$sql = "SELECT job_offer_accepted FROM job where job_id = :id_p";
												//exit($sql);

												$job_offer_accepted_v = getResultByID($sql, $job_id_v); //fetch() returns array
												//exit(print_r($job_offer_accepted_v));			

												foreach($job_offer_accepted_v as $offer_accepted=>$jobOfferAccepted) :
												//exit($jobOfferAccepted);
												endforeach;
												?>
												<input type="checkbox" name="job_offer_accepted" value="y"
															 <?php echo ($jobOfferAccepted=='y' ? 'checked="checked"' : '');?>> Offer Accepted
											</label>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">Notes:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control" maxlength="255" name="job_notes" value="<?php echo $job_notes_v ?>" />
										</div>
								</div>

								  <?php 
									$db = null; 
									?>

								<div class="form-group">
										<div class="col-sm-9 col-sm-offset-3">
												<button type="submit" class="btn btn-primary" name="edit" value="edit">Update</button>
										</div>
								</div>
						</form>
					</div>
			</div>
	</div>

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

		<!-- Turn off client-side validation, in order to test server-side validation.  -->
<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="../js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#edit_job').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					ssu_id: {
							validators: {
									notEmpty: {
											message: 'Site Supervisor required.'
									},
									stringLength: {
											min: 1,
											max: 5,
											message: 'Site Supervisor ID must be no more than 5 digits.'
									},
									regexp: {//see regexp examples below (here: must include 1-5 digits)
										regexp: /^[\d{1,5}]+$/,
										message: 'Can only contain numbers.'
									},									
							},
					},
					
					'skl_id[]': {//must use ' for arrays
							validators: {
									notEmpty: {
											message: 'Skill required.'
									},
									stringLength: {
											min: 1,
											max: 5,
											message: 'Skill ID must be no more than 5 digits.'
									},
									regexp: {
										regexp: /^[\d{1,5}]+$/,
										message: 'Can only contain numbers.'
									},									
							},
					},

					job_title: {
							validators: {
									notEmpty: {
											message: 'Title required.'
									},
									stringLength: {
											min: 1,
											max: 50,
											message: 'Title no more than 50 characters.'
									},
									regexp: {
										//http://www.regular-expressions.info/
										//http://www.regular-expressions.info/quickstart.html
										//http://www.regular-expressions.info/shorthand.html
										//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
										//upper-lower alphanumeric, hyphens, underscores, periods and spaces
										//regexp: /^[a-zA-Z0-9\-_\.\s]+$/ or "^[a-zA-Z0-9\-_\.\s]*$"
										//Note: * indicates characters not required (empty string)
								     //+ indicates must include one or more characters
										//similar to: (though, \w supports other Unicode characters)
										//Here we allow period (.) as well.
										/*
										^ : start of string
										[ : beginning of character group
											a-z : any lowercase letter
											A-Z : any uppercase letter
											0-9 : any digit
											_ : underscore
										] : end of character group
										* : zero or more of the given characters
										$ : end of string
										*/
										regexp: /^[\w\-\s\.]+$/,
										message: 'Letters, numbers, hyphens, periods, and underscores only allowed.'
									},									
							},
					},

					job_description: {
							validators: {
									notEmpty: {
											message: 'Description required.'
									},
									stringLength: {
											min: 1,
											max: 1000,
											message: 'Description no more than 1000 characters.'
									},
									regexp: {
										//upper-lower alphanumeric, hyphens, underscores, periods and spaces
										//regexp: /^[a-zA-Z0-9\-_\.\s]+$/ or "^[a-zA-Z0-9\-_\.\s]*$"
								     //+ indicates must include one or more characters
										//similar to: (though, \w supports other Unicode characters)
										//Here we allow period (.) as well.
										regexp: /^[\w\-\s\.,]+$/,
										message: 'Letters, numbers, hyphens, periods, underscores and commas only allowed.'
									},									
							},
					},

					job_semester: {
							validators: {
									notEmpty: {
											message: 'Semester required.'
									},
									stringLength: {
											min: 3,
											max: 4,
											message: 'Semester no more than 4 characters.'
									},
									regexp: {
										regexp: /^\b(fall|spr|sum)\b$/,																				
										message: 'Semester can only be fall, spr, or sum.'
									},									
							},
					},
 					
					job_year: {
							validators: {
									notEmpty: {
											message: 'Year required.'
									},
									stringLength: {
											min: 4,
											max: 4,
											message: 'Year must contain 4 digits.'
									},
									regexp: {
										regexp: /^[\d{4,4}]+$/,
										message: 'Can only contain 4-digit numbers.'
									},									
							},
					},

					job_recurrence: {
							validators: {
									notEmpty: {
											message: 'Recurrence required.'
									},
									stringLength: {
											min: 4,
											max: 10,
											message: 'Recurrence no more than 10 characters.'
									},
									regexp: {
										regexp: /^\b(none|semesterly|yearly)\b$/,
										message: 'Recurrence can only contain none, semesterly, or yearly.'
									},									
							},
					},
										
				job_credit_hours: {
							validators: {
									notEmpty: {
											message: 'Credit hours Required.'
									},
									stringLength: {
											min: 1,
											max: 1,
											message: 'Credit hours must be no more than 1 digit.'
									},
									regexp: {
										regexp: /^[1-6]+$/,
										message: 'Can only contain numbers 1 - 6.'
									},									
							},
					},
			}
	});
});
</script>

</body>
</html>
