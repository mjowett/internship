<?php include_once("../global/error_display.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="FSU's iSchool Online Internship System.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>FSU's iSchool Online Internship System</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="../css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("../global/header.php"); ?>	
						</div>

						<h2>Jobs</h2>

<a href="add_job.php">Add Job</a>
<br />

<?php
//make sure file is only required once, 
//fail causes error that stops remainder of page from processing
require_once "../global/connection.php";

//pull in function library
//require_once "../global/functions.php";

//call UDF (user-defined function)
//$rows = getJobs();

  try 
  {
  //get *all* jobs sorted by job id
  //Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table), ORDER BY has no effect!
  $sql =
		"select job_id, job_title, skl_id, skl_title
		 from job
				 natural join job_skill
				 natural join skill";

//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
  $statement = $db->prepare($sql);
  $statement->execute();
  $statement->setFetchMode(PDO::FETCH_ASSOC);
  $row = $statement->fetch(); //single row array

//exit(print_r($row));
?>
							
<!-- Responsive table.  -->						
<div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >
<thead>
  <tr>
<?php
foreach($row as $name => $value) :
?>
	<th><?php echo $name; ?></th>

<?php endforeach; ?>

  <th>&nbsp;</th>
  <th>&nbsp;</th>

  </tr>
</thead>

<?php
//for testing
//print_r($row);
//exit();

//foreach($results as $row) :
	/*
 Best practice: sanitize input (prepared statements), and escape output (htmlspecialchars())
		 
		 Call htmlspecialchars() when echoing data into HTML.
		 However, don't store escaped HTML in your database.
		 The database should store actual data, not its HTML representation.
		 Also, helps protect against cross-site scripting (XSS).
		 XSS enables attackers to inject client-side script into Web pages viewed by other users
	*/
?>

  <!-- Include table data here.  //-->

<?php
while($row)
{
?>

<tr>
<?php
foreach($row as $value) :
?>
	<td><?php echo htmlspecialchars($value); ?></td>	

<?php endforeach; ?>
	
  <!-- Create form button and hidden input fields to pass job info. to delete job.  //-->

  <td>
    <form 
    onsubmit="return confirm('Do you really want to delete record?');" 
    action="delete_job.php" 
    method="post" 
    id="delete_job">

      <input type="hidden" name="job_id" value="<?php echo $row['job_id']; ?>" />
      <input type="submit" value="Delete" />
    </form>
  </td>

  <!-- Create form button and hidden input fields to pass job and category info. to edit job.  //-->

  <td>
    <form action="edit_job.php" method="post" id="edit_job">

      <input type="hidden" name="job_id" value="<?php echo $row['job_id']; ?>" />
      <input type="submit" value="Edit" />
    </form>
  </td>

  </tr>

  <?php
$row = $statement->fetch();
}
	$statement->closeCursor();
  } 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    exit($error);
  }

  $db = null; 
  ?>

  </table>
  </div> <!-- end table-responsive -->

	<?php include_once "../global/footer.php"; ?>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>

		<script>
	 $(document).ready(function(){
		 $('#myTable').DataTable({
			 //https://datatables.net/reference/option/lengthMenu
			 //1st inner array page length values; 2nd inner array displayed options
			 //Note: -1 is used to disable pagination (i.e., display all rows)
			//Note: pageLength property automatically set to first value given in array
		 "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
	 //permit sorting (i.e., no sorting on last two columns: delete and edit)
    "columns":
		[
		null,
		null,
		null,
		null,			
     { "orderable": false },
     { "orderable": false }			
    ]
		 });
});
	</script>
	
</body>
</html>
