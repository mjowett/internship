<?php
//pull in database connection
require_once "global/connection.php";

$cus_id_v = 1;

// 1) create SQL statement with named placeholder (:cus_id_p), named placeholders begin with colon (:)
//The SQL statement can contain zero or more named (:name) or question mark (?) parameter markers 
//for which real values will be substituted when the statement is executed.

$query = 
"SELECT *
FROM customer
WHERE cus_id = :cus_id_p";

// 2) prepare() method: Prepares a statement for execution and returns PDOstatement object
$statement = $db->prepare($query);

// 3) bindValue(): Binds a value to a parameter (here: value in $cus_id_v) 
$statement->bindValue(':cus_id_p', $cus_id_v);

// 4) Executes a prepared statement
$statement->execute();

// 5) fetch(): Fetches (first or) next row from a result set (best to use fetch() than fetchAll() with large data sets)
//retrieve customer last name associated with $query (also, works well for many rows returned)
$result = $statement->fetch();
while($result != null)
{
  echo $result['cus_fname']. "<br />";
  echo $result['cus_lname']. "<br />";	
  $result = $statement->fetch();
}

// 6) Closes cursor, enabling statement to be executed again
$statement->closeCursor();

//connection still available until set to null
//$db = null; 


//________________________________________________________
//testing fetchAll():
$query = 
"SELECT *
FROM customer";

// 2) prepare() method: Prepares a statement for execution and returns PDOstatement object
$statement = $db->prepare($query);

// 3) not need to bind values

// 4) Executes a prepared statement
$statement->execute();

// 5) fetchAll(): 
$result = $statement->fetchAll();
//can also us var_dump
print_r($result);

// 6) Closes cursor, enabling statement to be executed again
$statement->closeCursor();

//________________________________________________________
// 7) resource clean-up: close connection, return memory back to server
$db = null; 
?>
