<?php
include_once("global/error_display.php");

//use for inital test of form inputs
//exit(print_r($_POST));

//add process code goes here...

//get item data
//no need for job_id when adding, uses auto increment
$emp_id_v = $_POST['emp_id'];

$ssu_fname_v = $_POST['ssu_fname'];
$ssu_lname_v = $_POST['ssu_lname'];
$ssu_phone_v = $_POST['ssu_phone'];
$ssu_email_v = $_POST['ssu_email'];

$ssu_notes_v = $_POST['ssu_notes'];

//emp_id: must include 1-5 digits
$pattern='/^[\d{1,5}]+$/';
$valid_emp_id_v = preg_match($pattern,$emp_id_v);
//echo $valid_emp_id_v; // test output: should be 1 (i.e., valid)

//fname
$pattern='/^[\w\-\s\.]+$/';
$valid_ssu_fname_v = preg_match($pattern,$ssu_fname_v);
//echo $valid_ssu_fname_id_v; // test output: should be 1 (i.e., valid)

//lname
$pattern='/^[\w\-\s\.]+$/';
$valid_ssu_lname_v = preg_match($pattern,$ssu_lname_v);
//echo $valid_ssu_lname_id_v; // test output: should be 1 (i.e., valid)
	
//phone
$pattern='/^\d{10}+$/';
$valid_ssu_phone_v = preg_match($pattern,$ssu_phone_v);
//echo $valid_job_credit_hours_v; // test output: should be 1 (i.e., valid)

if
(
	empty($emp_id_v) ||
	empty($ssu_fname_v) ||
	empty($ssu_lname_v) ||
	empty($ssu_phone_v) ||
	empty($ssu_email_v)
)
{
	$error = "Please check <span style='color:red; font-weight:bold;'>*</span>required fields and try again.";
	include('global/error.php');
}

else if ($valid_emp_id_v === false)
{
$error = 'Error in pattern';
include('global/error.php');
}
else if ($valid_emp_id_v === 0)
{
$error = 'emp_id can only include integers, from 1 to 5 digits';
include('global/error.php');
} 

else if ($valid_ssu_fname_v === false)
{
$error = 'Error in pattern';
include('global/error.php');
}
else if ($valid_ssu_fname_v === 0)
{
$error = 'fname can only contain letters, numbers, hyphens, periods, and underscores.';
include('global/error.php');
}
	
else if ($valid_ssu_lname_v === false)
{
$error = 'Error in pattern';
include('global/error.php');
}	
else if ($valid_ssu_lname_v === 0)
{
$error = 'lname can only contain letters, numbers, hyphens, periods, and underscores.';
include('global/error.php');
}	

else if ($valid_ssu_phone_v === false)
{
$error = 'Error in pattern';
include('global/error.php');
}
else if ($valid_ssu_phone_v === 0)
{
$error = 'Phone must be 10 digits';
include('global/error.php');
}

else
{
require_once "global/connection.php";

require_once "global/functions.php";

addSiteSupervisor
(
	$emp_id_v,
	$ssu_fname_v,
	$ssu_lname_v,
	$ssu_phone_v,
	$ssu_email_v,
	$ssu_notes_v
);

header('Location: index.php');
exit();	
}
?>
