<?php
include_once("global/error_display.php");

//delete code goes here

//get item ID
$ssu_id_v = $_POST['ssu_id'];

//validate input - must contain requuired field
if(empty($ssu_id_v))
{
	$error = "invalid data. check field and try again.";
	include('global/error.php');
}

else
{
	//if valid, delete
	//make sure file is only required once,
	//fail causes error that stops remainder of page from processing
	require_once('global/connection.php');
	
	//pull in function library
	require_once "global/functions.php";
	
	//call function, passing arguments that contain data values using variables
	deleteSiteSupervisor($ssu_id_v);
	
	include('index.php');
}
?>
