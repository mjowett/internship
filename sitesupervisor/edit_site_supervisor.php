<?php include_once("global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="FSU's iSchool Online Internship System.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>FSU's iSchool Online Internship System</title>

	<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<!-- Bootstrap for responsive, mobile-first design. -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- Note: following file is for form validation. -->
	<link rel="stylesheet" href="css/formValidation.min.css"/>

	<!-- Starter template for your own custom styling. -->
	<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		<?php include_once("global/nav.php"); ?>

		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h3>Edit Site Supervisor</h3>						
					<span style="color:red; font-weight: bold;">*</span> = required

					<form id="edit_site_supervisor" method="post" class="form-horizontal" action="edit_site_supervisor_process.php">

						<?php
						//make sure file is only required once
						require_once "global/connection.php";

						//pull in function library
						require_once "global/functions.php";

						//capture pst ID from index php page
						$ssu_id_v = $_POST['ssu_id'];
						//exit($ssu_id_v);

						?>

						<div class="form-group">
							<label class="col-sm-3 control-label">
								<span style="color:red; font-weight:bold;">*</span>Employer:
							</label>	
							<div class="col-sm-5">
								<?php
									//call UDF (user-defined function)
									//select ssu_if for specific job/internship
								$sql =
								"SELECT emp_id
								FROM employer
								natural join site_supervisor
								where emp_id = :id_p";
									//exit($sql);
									$emp_id_v = getResultSetByID($sql, $ssu_id_v); //fetchAll() returns array of arrays
									//exit(print_r($emp_id_v));

									//select all site supervisors (conpare ant matches below)
									$sql = "SELECT emp_id, emp_name
									FROM employer
									ORDER BY emp_name";
									$results = getResultSet($sql);
									//exit(print_r($results));
									?>

									<select name="emp_id">
										<?php
										foreach($results as $result):
											?>
										<option value="<?php echo $result['emp_id'];?>">
											<?php echo htmlspecialchars($result['emp_name']);?>
										</option>
										<?php
										endforeach;
										?>
									</select>
								</div>
							</div>

							<?php
							$query =
							"SELECT * 
							FROM site_supervisor
							WHERE ssu_id = :ssu_id_p";
							//exit($query);	

							$statement = $db->prepare($query);
							$statement->bindParam(':ssu_id_p', $ssu_id_v);
							$statement->execute();
							$result = $statement->fetch();

							$results = getSiteSupervisor($ssu_id_v);

							foreach ($results as $result) :
								?>

							<input type="hidden" name="ssu_id" value="<?php echo htmlspecialchars($result['ssu_id']); ?>" />

							<div class="form-group">
								<label class="col-sm-3 control-label">
									<span style="color:red; font-weight:bold;">*</span>First Name:
								</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" maxlength="15" name="ssu_fname" value="<?php echo $result['ssu_fname']; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">
									<span style="color:red; font-weight:bold;">*</span>Last Name:
								</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" maxlength="45" name="ssu_lname" value="<?php echo $result['ssu_lname']; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">
									<span style="color:red; font-weight:bold;">*</span>Phone:
								</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" maxlength="10" name="ssu_phone" value="<?php echo $result['ssu_phone']; ?>" />
								</div>
							</div>							

							<div class="form-group">
								<label class="col-sm-3 control-label">
									<span style="color:red; font-weight:bold;">*</span>Email:
								</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" maxlength="100" name="ssu_email" value="<?php echo $result['ssu_email']; ?>" />
								</div>
							</div>	

							<div class="form-group">
								<label class="col-sm-3 control-label">Notes:</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" maxlength="255" name="ssu_notes" value=" <?php echo $result['ssu_notes']; ?>" />
								</div>
							</div>

							<?php
							endforeach;
							$db = null;
							?>

						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="edit" value="edit">Update</button>

							</div>
						</div>
						
						
					</form>
				</div>
			</div>
		</div>

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

	<!-- Turn off client-side validation, in order to test server-side validation.  -->
	<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
	<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script type="text/javascript">
	$(document).ready(function()){
		${'#edit_site_supervisor').formValidation({
			message:'This value is not valid',
		icon: {//indicate which font Awesome icons to use
			valid: 'fa fa-check',
			invalid: 'fa fa-times',
			validating: 'fa fa-refresh'
		},
		fields: {
			emp_id {
				validators: {
					notEmpty: {
						message: 'Employer required.'
					},
					stringLength: {
						min: 1,
						max: 5,
						message: 'Employer ID must be no more than 5 digits'
					},
					regexp: {//see regexp examples below (here: must include 1-5 digits)
						regexp:/^[\d{1,5}]+$/,
						message: 'Can only contain numbers'
					},
				},
			},

			ssu_fname: {
				validators: {
					notEmpty: {
						message: 'Fname required.'
					},
					stringLength: {
						min: 1,
						max: 515,
						message: 'Fname no more than 15 charactes.'
					},
					regexp: {
						regexp:/^[\w\-\s\.]+$/,
						message: 'Letters, numbers, hypens, periods, and underscoreds only allowed.'
					},
				},
			},

			ssu_lname: {
				validators: {
					notEmpty: {
						message: 'Lname required.'
					},
					stringLength: {
						min: 1,
						max: 45,
						message: 'Fname no more than 45 charactes.'
					},
					regexp: {
						regexp:/^[\w\-\s\.]+$/,
						message: 'Letters, numbers, hypens, periods, and underscoreds only allowed.'
					},
				},
			},

			ssu_phone: {
				validators: {
					notEmpty: {
						message: 'Phone is required, including area code, only numbers'
					},
					stringLength: {
						min: 10,
						max: 10,
						message: 'Phone must be 10 digits'
					},
					regexp: {
						regexp: /^[0-9]+$/,
						message: 'Phone can only contain numbers'
					}
				},
			},

			ssu_email: {
				validators: {
					notEmpty: {
						message: 'Email required.'
					},
					stringLength: {
						min: 1,
						max: 100,
						message: 'Email no more than 100 characters.'
					},
					regexp: {
						regexp:/^[\w\-\s\.]+$/,
						message: 'Letters, numbers, hypens, periods, and underscoreds only allowed.'
					},
				},
			},
		});
});
</script>

</body>
</html>
