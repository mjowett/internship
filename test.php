<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="MySQL Database Server Information.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>MySQL Database Server Information</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("global/nav.php"); ?>
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<h2>MySQL Database Server Information</h2>
						</div>
						
<?php
require_once "./global/connection.php";

  try 
  {
  //get *all* jobs sorted by job id
  //Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)
  $query = "SELECT * FROM job ORDER BY job_id";

	//with large query result sets, loop through fetch() rather than fetchAll():
		//http://php.net/manual/en/pdostatement.bindcolumn.php
		
  //because no user entered data, no need to bind values
  $statement = $db->prepare($query);
  $statement->execute();
  $statement->setFetchMode(PDO::FETCH_ASSOC);
  $row = $statement->fetch();

?>	
<div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >
<thead>
	<tr>
<?php
//no need to hard code header names--automatically retrieves DB attribute name from SQL statement above!
foreach($row as $name => $value) :
?>
	<th><?php echo $name; ?></th>

<?php endforeach; ?>

</tr>
</thead>

<?php
while($row)
{
?>
<tr>
<?php
//likiewise, value for each attribute is automatically retrieved from SQL statement above!
foreach($row as $value) :
?>
	<td><?php echo $value; ?></td>	

<?php endforeach; ?>
	
</tr>
<?php
$row = $statement->fetch();
}
$statement->closeCursor();
} 

  catch (PDOException $e) 
  {
    $error = $e->getMessage();
    exit($error);
  }

$db = null; 
  ?>

	 </table>
 </div> <!-- end table-responsive -->

<?php
include_once "global/footer.php";
?>
		 </div>
	</div>
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script>
	 $(document).ready(function(){
    $('#myTable').DataTable();
});
	</script>

</body>
</html>
